var mongo = require('mongojs');

module.exports = function(config, connection) {
    connection = connection || 'connection';
    mongoURI = config[connection] + config.database.notifier;
    var db = mongo.connect(mongoURI, ['actions']);
    if (!db) {
        throw new Error('could not connect to ' + mongoURI);
    }

    return db;
};