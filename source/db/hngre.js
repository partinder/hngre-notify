var mongo = require('mongojs');

module.exports = function (config, connection) {
	connection = connection || 'connection';
	mongoURI = config[connection] + config.database.hngre;
	var db = mongo.connect(mongoURI,['users']);
	if (!db) {
		throw new Error('could not connect to ' + mongoURI);
	}

	return db;
};
