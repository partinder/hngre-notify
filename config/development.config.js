var config = {
    connection: 'mongodb://localhost:27017/',
    database: {
        hngre: 'db_hngre',
        notifier: 'notifier'
    },
    accessToken: '1234',

    hook: {
        url: 'http://localhost:5000/api/notify/',
        token: 'hngre-hook-token'
    },

    logentries: {
        token: null
    },

    mailOptions: {
        welcome: {
            to: "suresh.mahawar@hngre.com",
            from: "Hngre <hi@hngremail.com>",
            subject: "We’re stoked you’re here"
        },
        merchantInvitation: {
            to: "suresh.mahawar@hngre.com",
            from: "Hngre <hi@hngremail.com>"
        },
        friendInvitation: {
            to: "suresh.mahawar@hngre.com",
            from: "info@hngremail.com",
            subject: "Invitation"
        },
        hngreError: {
            to: "suresh.mahawar@hngre.com",
            from: "Hngre Error <info@hngremail.com>",
            subject: "Hngre Error"
        }
    },

    phoneOptions: {
        to: '+918802193481',
        from: '+12566158490',
    },

    transport: {
        mandrill: {
            token: 'fake-mandrill-api-token'
        },
        mailgun: {
            api_key: 'key-a184af3ea4a06a679240732867d0435d',
            domain: 'hngremail.com'
        },
        twilio: {
            accountSid: 'AC59b515897f70f729a9adf8d8d4f4938f',
            authToken: '24574be3b963db9b7cbf8024d59354d2'
        },
        gcm: {
            serverApiKey: 'AIzaSyC57jRzNHFAa6w4Sj7jlGLEdN_vl66BboQ'
        },
        apn: {
            cert: 'fake-cert-path',
            key: 'fake-key-path'
        }
    },

    jobs: {
        run: {
            resolve: 5,
            execute: 10
        },

        collection: 'notifierJobs'
    }
};

module.exports = config;