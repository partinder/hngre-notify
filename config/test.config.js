var config = {
    connection: 'mongodb://localhost:27017/',
    accessToken: '1234',
    database: {
        hngre: 'db_hngre_test',
        notifier: 'notifier'
    },

    logentries: {
        token: null
    },

    hook: {
        url: 'http://localhost:5000/api/notify/',
        token: 'fake-hook-token'
    },

    mailOptions: {
        welcome: {
            to: "suresh.mahawar@hngre.com",
            from: "Hngre <hi@hngremail.com>",
            subject: "We’re stoked you’re here"
        },
        merchantInvitation: {
            to: "suresh.mahawar@hngre.com",
            from: "Hngre <hi@hngremail.com>"
        },
        friendInvitation: {
            to: "suresh.mahawar@hngre.com",
            from: "info@hngremail.com",
            subject: "Invitation"
        }
    },

    phoneOptions: {
        to: '+918802193481',
        from: '+12566158490',
    },

    transport: {
        mandrill: {
            token: 'fake-mandrill-api-token'
        },
        mailgun: {
            api_key: 'fake-mailgun-api-token',
            domain: 'mydomain.mailgun.org'
        },
        twilio: {
            accountSid: 'fake-twilio-account-sid',
            authToken: 'fake-twilio-auth-token'
        },
        gcm: {
            serverApiKey: 'fake-google-server-api-key'
        },
        apn: {
            cert: 'fake-cert-path',
            key: 'fake-key-path'
        }
    },

    jobs: {
        run: {
            resolve: 5,
            execute: 10
        },

        collection: 'notifierJobs'
    }
};

module.exports = config;