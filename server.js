'use strict';
var notifier = require('./source/notifier');
var path = require('path');
var mongojs = require('mongojs');
var fs = require('fs');
var EmailTemplate = require('email-templates').EmailTemplate;
var templateDir = path.join(__dirname, 'templates', 'emails');
var messageDir = path.join(__dirname, 'templates', 'messages');
var config = require('./config');
var db = require('./source/db/hngre')(config);

notifier
    .receive('user-registered', function(e, actions, callback) {
        actions.create('send-welcome-email', {
            user: e.userId
        }, callback);
    })
    .resolve('send-welcome-email', function(a, actions, callback) {
        asyncRequestForUser(a.user, function(err, user) {
            if (err) {
                return callback(err);
            }

            actions.resolved(a, {
                email: user.email,
                name: user.first_name || ""
            }, callback);
        });
    })
    .execute('send-welcome-email', function(a, transport, callback) {
        var user = a.data;
        var welcomeDir = path.join(templateDir, "welcome");
        var welcomeEmail = new EmailTemplate(welcomeDir);

        welcomeEmail.render(user, function(err, results) {
            var data = {
                from: config.mailOptions.welcome.from,
                to: user.email,
                subject: config.mailOptions.welcome.subject,
                text: results.text,
                html: results.html
            };
            transport.mailgun.messages().send(data, callback);

        });
    });

notifier
    .receive('user-completed-action', function(e, actions, callback) {
        actions.create('send-android-push', {
            user: e.userId
        }, callback);
    })
    .resolve('send-android-push', function(a, actions, callback) {
        asyncRequestForUser(a.user, function(err, user) {
            if (err) {
                return callback(err);
            }

            actions.resolved(a, {
                deviceId: user.deviceId
            }, callback);
        });
    })
    .execute('send-android-push', function(a, transport, callback) {
        var regIds = [];
        regIds.push(a.data.regIds);

        var message = {
            title: 'This is a tite',
            message: "Hi there."
        };

        transport.android.push({
            message: message,
            regIds: regIds,
            retries: 3
        }, callback);
    });

notifier
    .receive('user-completed-action-with-hook', function(e, actions, callback) {
        actions.create('send-android-push', {
            user: e.userId
        }, callback);
    })
    .resolve('send-android-push', function(a, actions, callback) {
        asyncRequestForUser(a.user, function(err, user) {
            if (err) {
                return callback(err);
            }

            actions.resolved(a, {
                deviceId: user.deviceId
            }, callback);
        });
    })
    .execute('send-android-push', function(a, transport, callback) {
        var regIds = [];
        regIds.push(a.data.regIds);

        var message = {
            title: 'This is a tite',
            message: "Hi there."
        };

        transport.android.push({
            message: message,
            regIds: regIds,
            retries: 3
        }, function(err, result) {
            if (result.failure === 1) {
                var data = {
                    message: message,
                    status: result.success
                };

                notifier.sendHook('notify.sms', err || result, data);
            }

            return callback(err, result);
        });
    });

notifier
    .receive('user-completed-action', function(e, actions, callback) {
        actions.create('send-ios-push', {
            user: e.userId
        }, callback);
    })
    .resolve('send-ios-push', function(a, actions, callback) {
        asyncRequestForUser(a.user, function(err, user) {
            if (err) {
                return callback(err);
            }

            actions.resolved(a, {
                deviceId: user.deviceId
            }, callback);
        });
    })
    .execute('send-ios-push', function(a, transport, callback) {
        var tokens = [];
        tokens.push(a.data.token);

        transport.ios.push({
            production: false, // use specific gateway based on 'production' property.
            passphrase: 'secretPhrase',
            alert: {
                "body": "Your turn!",
                "action-loc-key": "Play",
                "launch-image": "mysplash.png"
            },
            badge: 1,
            tokens: tokens
        }, callback);
    });

//Send mail to merchants

notifier
    .receive('merchant-invite', function(e, actions, callback) {
        actions.create('send-merchant-invitation-email', {
            name: e.name,
            email: e.email
        }, callback);
    })
    .resolve('send-merchant-invitation-email', function(a, actions, callback) {

        actions.resolved(a, {
            name: a.name,
            email: a.email
        }, callback);
    })
    .execute('send-merchant-invitation-email', function(a, transport, callback) {
        var user = a.data;
        var subject = user.name + " is listed on Hngre";
        var merchantInvitationDir = path.join(templateDir, "merchant-invitation");
        var merchantInvitationEmail = new EmailTemplate(merchantInvitationDir);

        merchantInvitationEmail.render(user, function(err, results) {
            var data = {
                from: config.mailOptions.merchantInvitation.from,
                to: user.email,
                subject: subject,
                text: results.text,
                html: results.html
            };
            transport.mailgun.messages().send(data, callback);
        });
    });

//Send mail to friends
notifier.receive('friend-invite-email', function(e, actions, callback) {
    actions.create('send-invitation-email', {
        name: e.name,
        email: e.email
    });
}).resolve('send-invitation-email', function(a, actions, callback) {
    actions.resolved(a, {
        name: a.name,
        email: a.email
    }, callback);
}).execute('send-invitation-email', function(a, transport, callback) {
    var user = a.data;
    var friendInvitationDir = path.join(templateDir, "friend-invitation");
    var friendInvitationEmail = new EmailTemplate(friendInvitationDir);

    friendInvitationEmail.render(user, function(err, results) {
        var data = {
            from: config.mailOptions.friendInvitation.from,
            to: user.email,
            subject: config.mailOptions.friendInvitation.subject,
            text: results.text,
            html: results.html
        };
        transport.mailgun.messages().send(data, callback);
    });
});

//Send sms to friends
notifier
    .receive('friend-invite-sms', function(e, actions, callback) {
        actions.create('send-invite-sms', {
            phone: e.phone,
            deepLink: e.deepLink
        }, callback);
    })
    .resolve('send-invite-sms', function(a, actions, callback) {
        actions.resolved(a, {
            phone: a.phone,
            deepLink: a.deepLink
        }, callback);
    }).execute('send-invite-sms', function(a, transport, callback) {

        var friendInvitation = path.join(messageDir, "friend-invitation", "text.txt");
        fs.readFile(friendInvitation, 'utf8', function(err, data) {
            if (err) {
                return console.log(err);
            }
            var message = data.replace(/\{\{deepLink\}\}/g, a.data.deepLink);
            transport.twilio.messages.create({
                to: a.data.phone,
                from: config.phoneOptions.from,
                body: message,
            }, callback);
        });

    });


notifier.receive('hngre-error', function(e, actions, callback) {
    actions.create('send-error-mail', {
        name: e.name,
        email: e.email,
        errors: e.errors
    }, callback);
}).resolve('send-error-mail', function(a, actions, callback) {
    actions.resolved(a, {
        name: a.name,
        email: a.email,
        errors: a.errors
    }, callback);
}).execute('send-error-mail', function(a, transport, callback) {
    var error = a.data;

    var hngreErrorDir = path.join(templateDir, "hngre-error");
    var hngreErrorEmail = new EmailTemplate(hngreErrorDir);

    hngreErrorEmail.render(error, function(err, results) {
        var data = {
            from: config.mailOptions.hngreError.from,
            to: error.email,
            subject: config.mailOptions.hngreError.subject,
            text: results.text,
            html: results.html
        };
        transport.mailgun.messages().send(data, callback);
    });
});


notifier.start(process.env.NODE_PORT || 3031);

function asyncRequestForUser(userId, callback) {

    db.users.findOne({
        _id: mongojs.ObjectId(userId)
    }, function(err, user) {
        if (err) {
            console.log(err);
            throw err;
        }
        if (!user) {
            return callback({
                message: 'user not found'
            });
        }
        process.nextTick(function() {
            callback(null, user);
        });
    });
}