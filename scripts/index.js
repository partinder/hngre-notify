'use strict';
var csv = require("fast-csv"),
    request = require("request"),
    async = require('async'),
    users = [],
    sendMails = [],
    flag = false,
    file = "1st5000.test.csv",
    Joi = require('joi'),
    Boom = require('boom');

csv.fromPath(file)
    .on("data", function(data) {
        //console.log(data);
        if (flag) {
            users.push(data);
        }
        flag = true;
    })
    .on("end", function() {
        users.forEach(function(user) {
            if (user) {
                user = {
                    name: user[0],
                    email: user[1],
                    event: 'merchant-invite'
                };
                var schema = Joi.object().keys({
                    name: Joi.string(),
                    email: Joi.string().email().required(),
                    event: Joi.string().required()
                });

                Joi.validate(user, schema, function(err, value) {
                    if (!err) {
                        sendMails.push(user);
                    }
                });
            }
        });

        var errors = [];

        async.map(sendMails, function(user, callback) {
                request.post('http://localhost:3031/api/events?access_token=1234', {
                    form: user
                }, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var body = JSON.parse(body);
                        console.log(user.email, " - Done");
                        callback(null, body);
                    } else {
                        error = error || response.statusCode;
                        console.log(user.email, error);
                        error = new Error(error);
                        errors.push(Boom.wrap(error));
                        callback(error);
                    }
                });
            },
            function(err, results) {
                if (!err) {
                    console.log(results);
                } else {
                    console.log(err);
                }
                if (errors.length > 0) {
                    request.post('http://localhost:3031/api/events?access_token=1234', {
                        form: {
                            event: "hngre-error",
                            email: "suresh.mahawar@hngre.com",
                            errors: errors
                        }
                    }, function(error, response, body) {
                        if (!error && response.statusCode == 200) {
                            console.log("Sent Status Mail");
                        } else {
                            error = error || response.statusCode;
                            console.log(error, "Something wrong with status mail");
                        }
                    });
                }
            });
    });