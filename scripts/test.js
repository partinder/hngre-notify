/*Suresh,suresh.mahawar@hngre.com
Avih Rastogi,avihrastogi@gmail.com
Leon Thind,leon.thind@volunteerinindia.com
Harpreet Kaur,harpreet.kaur@in.ey.com
Suresh Mahawar,suresh.mahawar@technocube.in
Meher khanna,meher.khanna@aol.com
Akash Gupta,gupta_akash88@yahoo.co.in*/

'use strict';
var csv = require("fast-csv"),
    request = require("request"),
    async = require('async'),
    users = [],
    sendMails = [],
    flag = false,
    file = "1st5000.test.csv",
    Joi = require('joi'),
    Boom = require('boom');

if (!Array.prototype.asyncForEach) {
    Array.prototype.asyncForEach = function(each, done) {
        var i = -1,
            a = this;

        function iter() {
            if (++i === a.length) {
                done && done();
                return;
            }
            each.call(a, a[i], iter);
        }
        iter();
    };
}

csv.fromPath(file)
    .on("data", function(data) {
        //console.log(data);
        if (flag) {
            users.push(data);
        }
        flag = true;
    })
    .on("end", function() {
        users.forEach(function(user) {
            if (user) {
                user = {
                    name: user[0],
                    email: user[1],
                    event: 'merchant-invite'
                };
                var schema = Joi.object().keys({
                    name: Joi.string(),
                    email: Joi.string().email().required(),
                    event: Joi.string().required()
                });

                Joi.validate(user, schema, function(err, value) {
                    if (!err) {
                        sendMails.push(user);
                    }
                });
            }
        });

        var errors = [];
        sendMails.asyncForEach(function(user, next) {

            setTimeout(function() {
                if (!(user && user.email)) {
                    return next();
                }
                console.log(user.email, " - Start");
                request.post('http://localhost:3031/api/events?access_token=1234', {
                    form: user
                }, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var body = JSON.parse(body);
                        console.log(user.email, " - Done");
                        next();
                    } else {
                        error = error || response.statusCode;
                        console.log(user.email, error);
                        error = new Error(error);
                        errors.push(Boom.wrap(error));
                        next();
                    }
                });

                next();
            }, 1000);
        }, function() {
            console.log("all!");
            if (errors.length > 0) {
                request.post('http://localhost:3031/api/events?access_token=1234', {
                    form: {
                        event: "hngre-error",
                        email: "suresh.mahawar@hngre.com",
                        errors: errors
                    }
                }, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        console.log("Sent Status Mail");
                    } else {
                        error = error || response.statusCode;
                        console.log(error, "Something wrong with status mail");
                    }
                });
            }
        });
    });